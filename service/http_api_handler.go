// Package service provides HTTP and PubSub implementations
package service

import (
	"github.com/labstack/echo"
	"log"
	"net/http"
)

// HTTPApiHandler is a HTTP service structure with possible state
type HTTPApiHandler struct {
}

// Subscribe is an HTTP handler to create a subscription for topic/user tuple.
// Always returns 200
func (s *HTTPApiHandler) Subscribe(c *echo.Context) *echo.HTTPError {
	TopicsManager.Get(c.Param("topic")).Subscribe(c.Param("username"))
	return c.NoContent(http.StatusOK)
}

// UnSubscribe removes a subscription.
// Returns 200 for success, 404 if subscription did not exist
func (s *HTTPApiHandler) UnSubscribe(c *echo.Context) *echo.HTTPError {
	if err := TopicsManager.Get(c.Param("topic")).Unsubscribe(c.Param("username")); err != nil {
		return &echo.HTTPError{Code: http.StatusNotFound}
	}
	return c.NoContent(http.StatusOK)
}

// Poll returns next unseen message. It immediately returns if no message is available.
// Returns 200 on success, 204 if no messages available, 404 if subscription does not exist
func (s *HTTPApiHandler) Poll(c *echo.Context) *echo.HTTPError {
	if m, err := TopicsManager.Get(c.Param("topic")).Pop(c.Param("username")); err != nil {
		return &echo.HTTPError{Code: http.StatusNotFound, Message: err.Error()}
	} else if m == nil {
		return c.NoContent(http.StatusNoContent)
	} else {
		return c.String(http.StatusOK, m.Content)
	}
}

// PostMessage adds a message to a topic. The HTTP body is the message's content.
// It always returns 200 even if message was not added because of the empty body.
// The pubsub implementation will also not add a message if there are no subscribers
func (s *HTTPApiHandler) PostMessage(c *echo.Context) *echo.HTTPError {
	bbytes := make([]byte, c.Request.ContentLength)
	c.Request.Body.Read(bbytes)
	if len(bbytes) != 0 {
		log.Println("Received message:", string(bbytes))
		TopicsManager.Get(c.Param("topic")).Publish(string(bbytes))
	}
	return c.NoContent(http.StatusOK)
}
