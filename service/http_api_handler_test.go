package service

import (
	"fmt"
	"github.com/labstack/echo"
	mw "github.com/labstack/echo/middleware"
	"github.com/stretchr/testify/assert"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"
)

const (
	Msg1 = "a test message-1\n"
	Msg2 = "a test message-2\n"
	Msg3 = "a test message-3\n"
)

type th struct {
	e *echo.Echo
	h *HTTPApiHandler
}

var httpsvc = th{echo.New(), &HTTPApiHandler{}}

func init() {
	httpsvc.e.Use(mw.Logger)

	httpsvc.e.Post("/:topic", httpsvc.h.PostMessage)
	httpsvc.e.Post("/:topic/:username", httpsvc.h.Subscribe)
	httpsvc.e.Delete("/:topic/:username", httpsvc.h.UnSubscribe)
	httpsvc.e.Get("/:topic/:username", httpsvc.h.Poll)
}

//
func Test001_PollNotsubscribed(t *testing.T) {
	w := httptest.NewRecorder()
	r, _ := http.NewRequest("GET", fmt.Sprintf("/%v/%v", Topic1, User1), nil)
	httpsvc.e.ServeHTTP(w, r)
	assert.Equal(t, 404, w.Code)
}

//
func Test002_PollSubscribed(t *testing.T) {
	w := httptest.NewRecorder()

	r, _ := http.NewRequest("POST", fmt.Sprintf("/%v/%v", Topic1, User1), nil)
	httpsvc.e.ServeHTTP(w, r)
	assert.Equal(t, 200, w.Code)

	w = httptest.NewRecorder()
	r, _ = http.NewRequest("GET", fmt.Sprintf("/%v/%v", Topic1, User1), nil)
	httpsvc.e.ServeHTTP(w, r)
	assert.Equal(t, 204, w.Code)

	// cleanup subscriptions
	w = httptest.NewRecorder()
	r, _ = http.NewRequest("DELETE", fmt.Sprintf("/%v/%v", Topic1, User1), nil)
	httpsvc.e.ServeHTTP(w, r)
	assert.Equal(t, 200, w.Code)

	w = httptest.NewRecorder()
	r, _ = http.NewRequest("DELETE", fmt.Sprintf("/%v/%v", Topic1, User1), nil)
	httpsvc.e.ServeHTTP(w, r)
	assert.Equal(t, 404, w.Code)

}

func Test003_PublishPoll(t *testing.T) {
	w := httptest.NewRecorder()

	r, _ := http.NewRequest("POST", fmt.Sprintf("/%v/%v", Topic1, User1), nil)
	httpsvc.e.ServeHTTP(w, r)
	assert.Equal(t, 200, w.Code)

	w = httptest.NewRecorder()
	r, _ = http.NewRequest("GET", fmt.Sprintf("/%v/%v", Topic1, User1), nil)
	httpsvc.e.ServeHTTP(w, r)
	assert.Equal(t, 204, w.Code)

	w = httptest.NewRecorder()
	r, _ = http.NewRequest("POST", fmt.Sprintf("/%v", Topic1), strings.NewReader(Msg1))
	httpsvc.e.ServeHTTP(w, r)
	assert.Equal(t, 200, w.Code)

	w = httptest.NewRecorder()
	r, _ = http.NewRequest("GET", fmt.Sprintf("/%v/%v", Topic1, User1), nil)
	httpsvc.e.ServeHTTP(w, r)
	assert.Equal(t, 200, w.Code)

	b, _ := ioutil.ReadAll(w.Body)
	assert.Equal(t, Msg1, string(b))

	w = httptest.NewRecorder()
	r, _ = http.NewRequest("GET", fmt.Sprintf("/%v/%v", Topic1, User1), nil)
	httpsvc.e.ServeHTTP(w, r)
	assert.Equal(t, 204, w.Code)

	// cleanup subscriptions
	w = httptest.NewRecorder()
	r, _ = http.NewRequest("DELETE", fmt.Sprintf("/%v/%v", Topic1, User1), nil)
	httpsvc.e.ServeHTTP(w, r)
	assert.Equal(t, 200, w.Code)

}
