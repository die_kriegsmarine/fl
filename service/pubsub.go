// Package service ...
package service

import (
	"fmt"
)

// Message structure describes a single message on a specific topic
type Message struct {
	// Double-linked list
	next *Message
	prev *Message
	// Total number of subscribers which are to be notified at the time the message was published
	subscribers int
	// The actual message body
	Content string
}

// Topic structure defines a single subscription topic
type Topic struct {
	// Name of the topic
	Name string
	// Messages arranged as a double-linked list to maintain an O(1) complexity on item's insertion and removal
	Head *Message
	Tail *Message
	// List of current subscribers. The value points to a next message to be returned or nil
	Subscribers map[string]*Message
}

// Topics maps Topic structures to names
type Topics struct {
	topics map[string]*Topic
}

// TopicsManager is a singleton instance of Topics structure
var TopicsManager = &Topics{topics: make(map[string]*Topic)}

// Get returns existing topic or creates a new one
func (t *Topics) Get(name string) *Topic {
	if topic, found := t.topics[name]; found {
		return topic
	}

	topic := &Topic{Name: name, Subscribers: make(map[string]*Message)}
	t.topics[name] = topic

	return topic
}

// Publish adds a new message from a publisher only if there are subscribers for the topic
func (t *Topic) Publish(content string) {
	ls := len(t.Subscribers)
	if ls == 0 {
		return
	}
	m := &Message{subscribers: ls, Content: content}

	if t.Head == nil {
		t.Head, t.Tail = m, m
	} else if t.Tail != nil {
		t.Tail.next = m
		m.prev = t.Tail
	}

	t.Tail = m

	// TODO: want a constant time
	for k, v := range t.Subscribers {
		if v == nil {
			t.Subscribers[k] = m
		}
	}
}

// Pop returns last unread message for a subscriber and moves a subscription pointer.
// It returns a error if the subscription does not exist
func (t *Topic) Pop(username string) (*Message, error) {
	if m, exists := t.Subscribers[username]; exists {
		if m != nil {
			m.subscribers--
			t.Subscribers[username] = m.next

			if m.subscribers == 0 {
				t.remove(m)
			}
			return m, nil
		}
		return nil, nil
	}
	return nil, fmt.Errorf("subscription not found")
}

// Subscribe creates a new subscription. Returns error if the subscription exists
func (t *Topic) Subscribe(username string) error {
	if _, exists := t.Subscribers[username]; exists {
		return fmt.Errorf("subscription already exists")
	}
	t.Subscribers[username] = nil
	return nil
}

// Unsubscribe removes a subscription and also releases all the messages, for which this was a sole
// subscriber. Returns error if subscription does it exist
func (t *Topic) Unsubscribe(username string) error {
	if m, exists := t.Subscribers[username]; exists {
		for m != nil {
			m.subscribers--
			if m.subscribers == 0 {
				t.remove(m)
			}
			m = m.next
		}
		delete(t.Subscribers, username)
		return nil
	}
	return fmt.Errorf("subscription does not exist")
}

// remove deletes a message from a double-linked list.
// Not resetting .next and .prev to nil should be safe even for a concurrent implementations:
// the operation guarantees no other node is pointing to the removed node, so GC should recycle it just fine.
// For a high-concurrent environment though it's better to reset .next and .prev to make GC more efficient
func (t *Topic) remove(m *Message) {
	//delete message
	if t.Head == m {
		t.Head = m.next
	}

	if t.Tail == m {
		t.Tail = m.prev
	}

	if m.prev != nil {
		m.prev.next = m.next
	}

	if m.next != nil {
		m.next.prev = m.prev
	}
}

// Len give a current size of unread messages
// Mostly useful for testing
func (t *Topic) Len() (sz uint32) {
	for m := t.Head; m != nil; m = m.next {
		sz++
	}
	return sz
}
