package service

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

const (
	Topic1 = "testTopic1"
	Topic2 = "testTopic2"
	User1  = "testUser1"
	User2  = "testUser2"
	User3  = "testUser3"
	User4  = "testUser4"
)

// simple topic creation test
func Test001_GetTopic(t *testing.T) {
	topic := TopicsManager.Get(Topic1)
	assert.NotNil(t, topic)
	assert.Equal(t, topic, TopicsManager.Get(Topic1))
	assert.Equal(t, topic, TopicsManager.Get(Topic1))
	assert.Equal(t, uint32(0), topic.Len())
}

// test un/subscription
func Test002_SubscribeUnsubscribe(t *testing.T) {
	topic := TopicsManager.Get(Topic1)
	assert.Len(t, TopicsManager.topics, 1)
	assert.Empty(t, topic.Subscribers)

	err := topic.Subscribe(User1)
	assert.NoError(t, err)
	assert.Len(t, topic.Subscribers, 1)
	assert.Equal(t, uint32(0), topic.Len())

	m, err := topic.Pop(User1)
	assert.Nil(t, m)
	assert.NoError(t, err)

	m, err = topic.Pop(User2)
	assert.Nil(t, m)
	assert.Error(t, err)

	topic.Unsubscribe(User1)
	assert.Len(t, topic.Subscribers, 0)
}

// test simple publishing
func Test003_Publish(t *testing.T) {
	assert.Len(t, TopicsManager.topics, 1)
	topic := TopicsManager.Get(Topic1)

	assert.Len(t, topic.Subscribers, 0)
	assert.Equal(t, uint32(0), topic.Len())
	topic.Publish("msg1")
	assert.Equal(t, uint32(0), topic.Len())

	topic.Subscribe(User1)
	defer topic.Unsubscribe(User1)
	topic.Publish("msg1")
	assert.Equal(t, uint32(1), topic.Len())

	m, err := topic.Pop(User1)
	assert.Equal(t, uint32(0), topic.Len())
	assert.NoError(t, err)
	assert.NotNil(t, m)
	assert.Equal(t, "msg1", m.Content)

}

// test bounds publishing / popping for a single subscriber
func Test004_OneSubscriberBounds(t *testing.T) {
	assert.Len(t, TopicsManager.topics, 1)
	topic := TopicsManager.Get(Topic1)

	assert.Len(t, topic.Subscribers, 0)
	m, err := topic.Pop(User1)
	assert.Error(t, err)
	assert.Nil(t, m)

	topic.Subscribe(User1)
	defer topic.Unsubscribe(User1)

	topic.Publish("msg1")
	topic.Publish("msg2")
	topic.Publish("msg3")

	assert.Equal(t, uint32(3), topic.Len())

	assert.Equal(t, 1, topic.Head.subscribers)
	assert.Equal(t, 1, topic.Head.next.subscribers)
	assert.Equal(t, 1, topic.Head.next.next.subscribers)

	assert.Nil(t, topic.Head.next.next.next)
	assert.Nil(t, topic.Head.prev)

	m, err = topic.Pop(User1)
	assert.NoError(t, err)
	assert.NotNil(t, m)
	assert.Equal(t, "msg1", m.Content)
	assert.Equal(t, uint32(2), topic.Len())

	m, err = topic.Pop(User1)
	assert.NoError(t, err)
	assert.NotNil(t, m)
	assert.Equal(t, "msg2", m.Content)
	assert.Equal(t, uint32(1), topic.Len())

	m, err = topic.Pop(User1)
	assert.NoError(t, err)
	assert.NotNil(t, m)
	assert.Equal(t, "msg3", m.Content)
	assert.Equal(t, uint32(0), topic.Len())

	m, err = topic.Pop(User1)
	assert.NoError(t, err)
	assert.Nil(t, m)
	assert.Equal(t, uint32(0), topic.Len())

	topic.Publish("msg1")
	m, err = topic.Pop(User1)
	assert.NoError(t, err)
	assert.NotNil(t, m)
	assert.Equal(t, "msg1", m.Content)
	assert.Equal(t, uint32(0), topic.Len())

}

// test un/subscription and topic's message management
func Test005_OneSubscribeUnsubscribe(t *testing.T) {
	assert.Len(t, TopicsManager.topics, 1)
	topic := TopicsManager.Get(Topic1)

	assert.Len(t, topic.Subscribers, 0)
	topic.Subscribe(User1)

	topic.Publish("msg1")
	topic.Publish("msg2")
	topic.Publish("msg3")

	assert.Equal(t, uint32(3), topic.Len())

	topic.Pop(User1)
	assert.Equal(t, uint32(2), topic.Len())

	m, err := topic.Pop(User2)
	assert.Error(t, err)
	assert.Nil(t, m)

	topic.Unsubscribe(User1)
	assert.Len(t, topic.Subscribers, 0)
	assert.Equal(t, uint32(0), topic.Len())
}

// test multiple subscribers and topic's message management
func Test006_MultipleSubscribers(t *testing.T) {
	assert.Len(t, TopicsManager.topics, 1)
	topic := TopicsManager.Get(Topic1)

	assert.Equal(t, uint32(0), topic.Len())
	assert.Len(t, topic.Subscribers, 0)

	topic.Subscribe(User1)
	topic.Publish("msg1")
	topic.Subscribe(User2)
	topic.Publish("msg2")
	topic.Subscribe(User3)
	topic.Publish("msg3")

	assert.Equal(t, uint32(3), topic.Len())

	m, err := topic.Pop(User3)
	assert.Nil(t, err)
	assert.NotNil(t, m)
	assert.Equal(t, 2, m.subscribers)
	assert.Equal(t, uint32(3), topic.Len())

	//no more messages for User3
	m, err = topic.Pop(User3)
	assert.Nil(t, err)
	assert.Nil(t, m)

	// publish another message
	topic.Publish("msg4")
	assert.Equal(t, uint32(4), topic.Len())

	//unsubscribe User1
	topic.Unsubscribe(User1)
	assert.Equal(t, uint32(3), topic.Len())

	// User2
	m, err = topic.Pop(User2)
	assert.Nil(t, err)
	assert.NotNil(t, m)
	assert.Equal(t, 0, m.subscribers)
	assert.Equal(t, "msg2", m.Content)
	assert.Equal(t, uint32(2), topic.Len())

	// another one for User2
	m, err = topic.Pop(User2)
	assert.Equal(t, uint32(1), topic.Len())

	// unsubscribe User2
	topic.Unsubscribe(User2)
	assert.Equal(t, uint32(1), topic.Len())

	topic.Unsubscribe(User3)

	assert.Len(t, topic.Subscribers, 0)
}

// simple multi-topic subscription, publishing, unsubscription
func Test007_MultipleTopics(t *testing.T) {
	topic1 := TopicsManager.Get(Topic1)
	topic2 := TopicsManager.Get(Topic2)
	assert.NotEqual(t, topic1, topic2)

	assert.Equal(t, uint32(0), topic1.Len())

	assert.Len(t, TopicsManager.topics, 2)

	topic1.Subscribe(User1)
	topic1.Publish("msg1")
	topic1.Subscribe(User2)
	topic1.Publish("msg2")
	err := topic1.Subscribe(User2)
	assert.Error(t, err)

	topic2.Subscribe(User1)
	topic2.Subscribe(User3)
	topic2.Publish("msg1")
	topic2.Publish("msg2")

	assert.Equal(t, uint32(2), topic1.Len())
	assert.Equal(t, uint32(2), topic2.Len())

	topic1.Unsubscribe(User1)
	assert.Equal(t, uint32(1), topic1.Len())

	topic1.Pop(User2)
	assert.Equal(t, uint32(0), topic1.Len())

	topic2.Unsubscribe(User1)
	assert.Equal(t, uint32(2), topic2.Len())

	topic2.Unsubscribe(User3)
	assert.Equal(t, uint32(0), topic2.Len())

	assert.Len(t, topic1.Subscribers, 1)
	assert.Len(t, topic2.Subscribers, 0)

	topic1.Unsubscribe(User2)
	assert.Len(t, topic1.Subscribers, 0)

}

// complete test coverage - missing case
func Test008_PopMidList(t *testing.T) {
	topic := TopicsManager.Get(Topic1)

	assert.Equal(t, uint32(0), topic.Len())
	assert.Len(t, topic.Subscribers, 0)

	topic.Subscribe(User1)
	topic.Publish("msg1")
	topic.Publish("msg2")
	topic.Publish("msg3")

	topic.remove(topic.Head.next)

	//
	m, err := topic.Pop(User1)
	assert.Nil(t, err)
	assert.NotNil(t, m)
	assert.Equal(t, "msg1", m.Content)
	assert.Equal(t, uint32(1), topic.Len())

	//
	m, err = topic.Pop(User1)
	assert.Nil(t, err)
	assert.NotNil(t, m)
	assert.Equal(t, "msg3", m.Content)
	assert.Equal(t, uint32(0), topic.Len())

	topic.Unsubscribe(User1)
	assert.Equal(t, uint32(0), topic.Len())
	assert.Len(t, topic.Subscribers, 0)
}
