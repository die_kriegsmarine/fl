// Package main initializes an HTTP server, creates routes, and start the server on a default port
package main

import (
	"bitbucket.org/die_kriegsmarine/fl/service"
	"github.com/labstack/echo"
	mw "github.com/labstack/echo/middleware"
	"log"
)

func main() {

	e := echo.New()
	e.Use(mw.Logger)

	h := service.HTTPApiHandler{}
	e.Post("/:topic", h.PostMessage)
	e.Post("/:topic/:username", h.Subscribe)
	e.Delete("/:topic/:username", h.UnSubscribe)
	e.Get("/:topic/:username", h.Poll)

	log.Println("Starting server on port 8080")
	e.Run(":8080")
}
