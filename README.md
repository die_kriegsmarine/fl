# A Publish/Subscribe Server exercise

## Prerequisites

Golang 1.3+ should be installed on a system. Download and install Go from [here](http://golang.org/doc/install). These instructions assume using a UNIX-like OS for simplicity.

## Getting and running the source code

Make sure your `GOPATH` is set. In terminal window, issue:

>	`go get bitbucket.org/die_kriegsmarine/fl`

>	`go install bitbucket.org/die_kriegsmarine/fl`

Now you can run the code with:

>	`$GOPATH/bin/fl`


## Implementation notes

For simplicity and clarity, the service does not support concurrent operations. Adding support for that will require using mutexes, which will affect the code brevity.

Also, the implementation is not optimal. One of the possible things to improve is an unsubscribe operation as currently it has complexity O(N). 

## Testing notes

Code testing converage is 100%.

